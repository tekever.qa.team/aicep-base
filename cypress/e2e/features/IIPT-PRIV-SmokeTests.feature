# language: pt
Funcionalidade: Invest in Portugal - Página Privada - Smoke Tests 

Contexto: Utilizador da área privada deve estar logado
        Dado Aceder à área "Privada" do "IIPT"
        Quando Preencher Email e Password
        Então Clicar no botão Connect


Cenário: Validar a página Portugal in the World da página privada
        Dado Clicar no menu "Portugal in the World"
        Quando  Validar módulo "Main Nav"
        E Validar módulo "Simple Header" 
        E Validar módulo "Data Comparator" 
        E Validar módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Success Stories da página privada
        Dado Clicar no menu "Success Stories"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid"
        E Validar módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Story Detail da página privada
        Dado Clicar no menu "Story Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Stepped Features"
        E Validar módulo "Wall Of Text"
        E Validar módulo "Testimonials"
        E Validar módulo "Card Slider List"
        Então Validar módulo "Footer Navigation"
        
Cenário: Validar a página Our Industries da página privada
        Dado Clicar no menu "Our Industries"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Standard Grid"
        E Validar módulo "Dual Banner"
        E Validar módulo "Card Slider List"
        E Validar módulo "Dual Banner"  
        E Validar um segundo módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Industry Detail da página privada
        Dado Clicar no menu "Industry Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Wall Of Text"
        E Validar módulo "Stepped Features"
        E Validar um segundo módulo "Stepped Features"
        E Validar módulo "Single Banner"
        E Validar um segundo módulo "Wall Of Text"
        E Validar módulo "Card Slider List"
        E Validar módulo "Case Card"
        E Validar um segundo módulo "Card Slider List"
        E Validar módulo "Reading Text Card"  
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Case Study Detail da página privada
        Dado Clicar no menu "Case Study Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Stepped Features"
        E Validar módulo "Wall Of Text"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Available Properties da página privada
        Dado Clicar no menu "Available Properties"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid" 
        E Validar módulo "Reading Image Card"
        E Validar módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Events da página privada
        Dado Clicar no menu "Events"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid"        
        E Validar módulo "Reading Image Card" 
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Event Detail da página privada
        Dado Clicar no menu "Event Detail"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Reading Header"
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Suppliers da página privada
        Dado Clicar no menu "Suppliers"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid" 
        E Validar módulo "Supplier Card"
        E Validar módulo "Card Slider List"
        E Validar módulo "Reading Image Card"
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Our Regions da página privada
        Dado Clicar no menu "Our Regions"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Interactive Header"
        E Validar módulo "Sidebar Filtered Grid"
        E Validar módulo "Dual Banner"
        E Validar módulo "Card Slider List"
        Então Validar módulo "Footer Navigation"   

Cenário: Validar a página Region Detail da página privada
        Dado Clicar no menu "Region Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Region Header"
        E Validar módulo "Standard Grid"
        E Validar módulo "Info Numbers"
        E Validar módulo "Collapsed Indicator"
        E Validar módulo "Single Banner"
        E Validar módulo "Card Slider List"
        E Validar módulo "Item Card"
        E Validar módulo "Card Slider Info"
        E Validar módulo "Reading Image Card"
        E Validar módulo "Wall Of Text"
        E Validar módulo "Media Slider"
        E Validar módulo "Text Heading"
        E Validar módulo "Media Content Slider"
        E Validar um segundo módulo "Wall Of Text"    
        E Validar um segundo módulo "Single Banner"       
        Então Validar módulo "Footer Navigation"   
        
Cenário: Validar a página Find Spot to invest da página privada
        Dado Clicar no menu "Find Spot To Invest"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header" 
        E Validar módulo "Find Spot Form" 
        Então Validar módulo "Footer Navigation"