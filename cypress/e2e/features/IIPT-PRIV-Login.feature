# language: pt
@login
Funcionalidade: Invest in Portugal - Página Privada - Login
    Cenário: Fazer login na área privada do IIPT
        Dado Aceder à área "Privada" do "IIPT"
        Quando Preencher Email e Password
        E Clicar no botão Connect
        Então Visualizar homepage da área privada

# Cenário: Fazer login na área privada do IIPT com LinkedIn
#         Dado Aceder à área "Privada" do "BFPT"
#         Quando Clicar no botão LinkedIn
#         E Sign In no LinkedIn
#         # Então Visualizar homepage da área privada

# Cenário: Fazer login na área privada do IIPT com Google
#         Dado Aceder à área "Privada" do "IIPT"
#         Quando Clicar no botão Google
#         # E Sign In no Google
#         Então Visualizar homepage da área privada