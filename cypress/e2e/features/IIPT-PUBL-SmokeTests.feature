# language: pt
Funcionalidade: Invest in Portugal - Página Pública - Smoke Tests


Cenário: Validar a página Portugal in the World da página pública
        Dado Aceder a página pública "Portugal in the World"
        Quando  Validar módulo "Main Nav"
        E Validar módulo "Simple Header" da página pública 
        E Validar módulo "Data Comparator" da página pública
        E Validar módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Success Stories da página pública
        Dado Aceder a página pública "Success Stories"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid With Sort"
        E Validar módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Story Detail da página pública
        Dado Aceder a página pública "Story Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Stepped Features"
        E Validar módulo "Wall Of Text"
        E Validar módulo "Testimonials"
        E Validar módulo "Card Slider List"
        Então Validar módulo "Footer Navigation"
        
Cenário: Validar a página Our Industries da página pública
        Dado Aceder a página pública "Our Industries"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Standard Grid"
        E Validar módulo "Dual Banner"
        E Validar módulo "Card Slider List"
        E Validar módulo "Dual Banner"  
        E Validar um segundo módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Industry Detail da página pública
        Dado Aceder a página pública "Industry Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Wall Of Text"
        E Validar módulo "Stepped Features"
        E Validar um segundo módulo "Stepped Features"
        E Validar módulo "Single Banner"
        E Validar um segundo módulo "Wall Of Text"
        E Validar módulo "Card Slider List"
        E Validar módulo "Case Card"
        E Validar um segundo módulo "Card Slider List"
        E Validar módulo "Reading Text Card"  
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Case Studies da página pública
         Dado Aceder a página pública "Case Studies"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid With Sort"
        E Validar módulo "Case Card"
        E Validar módulo "Single Banner"
        Então Validar módulo "Footer Navigation"

Cenário: Validar a página Case Study Detail da página privada
         Dado Aceder a página pública "Case Study Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header"
        E Validar módulo "Stepped Features"
        E Validar módulo "Wall Of Text"
        Então Validar módulo "Footer Navigation"   

Cenário: Validar a página Events da página pública
        Dado Aceder a página pública "Events"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid"        
        E Validar módulo "Reading Image Card" 
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Event Detail da página pública
       Dado Aceder a página pública "Event Detail"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Reading Header"
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Suppliers da página pública
        Dado Aceder a página pública "Suppliers"
        Quando Validar módulo "Main Nav"        
        E Validar módulo "Simple Header"
        E Validar módulo "Filtered Grid" 
        E Validar módulo "Supplier Card"
        E Validar módulo "Card Slider List"
        E Validar módulo "Reading Image Card"
        Então Validar módulo "Footer Navigation"    

Cenário: Validar a página Our Regions da página pública
        Dado Aceder a página pública "Our Regions"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Interactive Header" da página pública
        E Validar módulo "Sidebar Filtered Grid" da página pública
        E Validar módulo "Dual Banner"
        E Validar módulo "Card Slider List"
        Então Validar módulo "Footer Navigation"   

Cenário: Validar a página Region Detail da página pública
        Dado Aceder a página pública "Region Detail"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Region Header" da página pública
        E Validar módulo "Standard Grid" da página pública
        E Validar módulo "Info Numbers" da página pública
        E Validar módulo "Collapsed Indicator" da página pública
        E Validar módulo "Single Banner" da página pública
        E Validar módulo "Card Slider List" da página pública
        E Validar módulo "Item Card" da página pública
        E Validar módulo "Card Slider Info"
        E Validar módulo "Reading Image Card"
        E Validar módulo "Wall Of Text"
        E Validar módulo "Media Slider"
        E Validar módulo "Text Heading"
        E Validar módulo "Media Content Slider"
        E Validar um segundo módulo "Wall Of Text"    
        E Validar um segundo módulo "Single Banner"       
        Então Validar módulo "Footer Navigation"   
        
Cenário: Validar a página Find Spot to invest da página pública
        Dado Aceder a página pública "Find Spot To Invest"
        Quando Validar módulo "Main Nav"
        E Validar módulo "Simple Header" da página pública 
        E Validar módulo "Find Spot Form" da página pública 
        Então Validar módulo "Footer Navigation"