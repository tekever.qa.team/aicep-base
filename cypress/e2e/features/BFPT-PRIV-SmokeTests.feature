# language: pt
Funcionalidade: Buy From Portugal - Página Privada - Smoke Tests

Cenário: Validar a página Portugal in the World da página pública
        Dado Aceder a página pública "Portugal in the World"
        Quando  Validar módulo "Main Nav"
        E Validar módulo "Simple Header" da página pública
        E Validar módulo "Data Comparator" da página pública
        E Validar módulo "Dual Banner"
        Então Validar módulo "Footer Navigation"

# Cenário: Validar a página Find Spot to invest da página pública
#         Dado Aceder a página pública "Find Spot to invest"
#         Quando Validar módulo "Main Nav"
#         E Validar módulo "Simple Header" da página pública
#         E Validar módulo "Find Spot Form" da página pública
#         Então Validar módulo "Footer Navigation"

# Cenário: Validar a página Our Regions da página pública
#         Dado Aceder a página pública "Our Regions"
#         Quando Validar módulo "Main Nav"
#         E Validar módulo "Interactive Header"
#         E Validar módulo "Sidebar Filtered Grid"
#         E Validar módulo "Dual Banner"
#         E Validar módulo "Card Slider List"
#         Então Validar módulo "Footer Navigation"    

# Cenário: Validar a página Our Industries da página pública
#         Dado Aceder a página pública "Our Industries"
#         Quando Validar módulo "Main Nav"
#         E Validar módulo "Simple Header"
#         E Validar módulo "Standard Grid"
#         E Validar módulo "Dual Banner"
#         E Validar módulo "Card Slider List"
#         E Validar módulo "Dual Banner"  
#         E Validar um segundo módulo "Dual Banner"
#         Então Validar módulo "Footer Navigation"

# Cenário: Validar a página Success Stories da página pública
#         Dado Aceder a página pública "Success Stories"
#         Quando Validar módulo "Main Nav"
#         E Validar módulo "Simple Header"
#         E Validar módulo "Filtered Grid With Sort"
#         E Validar módulo "Dual Banner"
#         Então Validar módulo "Footer Navigation"
        
# Cenário: Validar a página Suppliers da página pública
#         Dado Aceder a página pública "Suppliers"
#         Quando Validar módulo "Main Nav"        
#         E Validar módulo "Simple Header"
#         E Validar módulo "Filtered Grid" 
#         E Validar módulo "Supplier Card"
#         E Validar módulo "Card Slider List"
#         E Validar módulo "Reading Image Card"
#         Então Validar módulo "Footer Navigation"      

# Cenário: Validar a página Region Detail da página pública
#         Dado Aceder a página pública "Region Detail"
#         Quando Validar módulo "Main Nav"
#         E Validar módulo "Region Header"
#         E Validar módulo "Standard Grid"
#         E Validar módulo "Info Numbers"
#         E Validar módulo "Collapsed Indicator"
#         E Validar módulo "Single Banner"
#         E Validar módulo "Card Slider List"
#         E Validar módulo "Item Card"
#         E Validar módulo "Card Slider Info"
#         E Validar módulo "Reading Image Card"
#         E Validar módulo "Wall Of Text"
#         E Validar módulo "Media Slider"
#         E Validar módulo "Text Heading"
#         E Validar módulo "Media Content Slider"
#         E Validar um segundo módulo "Wall Of Text"    
#         E Validar um segundo módulo "Single Banner"       
#         Então Validar módulo "Footer Navigation"