
// /// <reference types="Cypress" />
// import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
// let arealink = null;
// let projetolink = null;
// let dotlink = null;


// Given('I navigate to Invest in Portugal Private Page', function () {
//     cy.visit('https://qa-myaicep.investinportugal.pt/');
// })
// When('I enter login credentials of Douglas', function () {
//     cy.login('douglas.silva@tekever.com', 'Aicep2023@test')
// })

// Given('I navigate to Invest in Portugal Public Page', function () {
//     cy.visit('https://qa.investinportugal.pt/');
// })

// When('Search by nav bar', () => {
//     cy.fixture('IIPT/public/header.json').then((locators) => {
//         cy.get(locators.btnSearch).click()
//         cy.get(locators.inputSearch).type('lisboa')
//         cy.get(locators.btnSearchSubmit).click()
//     })
// })

// Then('user should be able to search', function () {
//     cy.search('lisboa')
// })


// Then('user should be able to download a document', function () {
//     cy.fixture('private/index_private.json').then((locatorsIndex) => {
//         cy.get(locatorsIndex.btnUsefulDocuments).click()
//     })
//     cy.fixture('IIPT/private/useful_documents.json').then((locatorsUsefulDocs) => {
//         cy.get(locatorsUsefulDocs.btnDownloadDoc1).click()
//     })
// })


// When("I type in {string} and submit", (boardName) => {
//     cy.get("[data-cy=first-board]").type(`${boardName}{enter}`);
// });

// Given("Aceder à área {string} do {string}", function (area, projeto) {


//     if (projeto == 'IIPT') {
//         projetolink = 'investinportugal';
//         dotlink = 'pt';
//     } else {
//         projetolink = 'buyfromportugal';
//         dotlink = 'com';
//     }

//     if (area == 'Privada') {
//         arealink = '-myaicep';
//     } else {
//         arealink = '';
//     }
//     cy.visit('https://' + Cypress.env('server') + arealink + '.' + projetolink + '.' + dotlink);
//     cy.log('https://' + Cypress.env('server') + arealink + '.' + projetolink + '.' + dotlink);
// });

// When('Preencher Email e Password', function () {
//     // const i = randomInteger(0, 1);
//     cy.fixture('data/userFile.json')
//         .then((userFile) => {
//             cy.loginPrivateIIPT(userFile.userIIPT[1].email, userFile.userIIPT[1].password)
//         })
// })

// // function randomInteger(min, max) {
// //     return Math.floor(Math.random() * (max - min + 1)) + min;
// // }

// Then('Clicar no botão Connect', function () {
//     cy.clickBtnConnectMe();
// })

// Then('Visualizar homepage da área privada', function () {
//     cy.welcomeIIPT();
// })

// When('Clicar no botão LinkedIn', function () {
//     cy.clickLinkedin();
// })
// When('Sign In no LinkedIn', function () {
//     //  cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
//     cy.window().then((win) => {
//         cy.stub(win, 'open', url => {
//             win.location.href = 'https://www.linkedin.com/uas/login?session_redirect=%2Foauth%2Fv2%2Flogin-success%3Fapp_id%3D207679828%26auth_type%3DAC%26flow%3D%257B%2522scope%2522%253A%2522r_liteprofile%2Br_emailaddress%2522%252C%2522state%2522%253A%2522_linkedin%2522%252C%2522appId%2522%253A207679828%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522https%253A%252F%252Fqa-myaicep.investinportugal.pt%252Flinkedin%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522currentSubStage%2522%253A0%252C%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522creationTime%2522%253A1689083941751%257D&fromSignIn=1&trk=oauth&cancel_redirect=%2Foauth%2Fv2%2Flogin-cancel%3Fapp_id%3D207679828%26auth_type%3DAC%26flow%3D%257B%2522scope%2522%253A%2522r_liteprofile%2Br_emailaddress%2522%252C%2522state%2522%253A%2522_linkedin%2522%252C%2522appId%2522%253A207679828%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522https%253A%252F%252Fqa-myaicep.investinportugal.pt%252Flinkedin%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522currentSubStage%2522%253A0%252C%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522creationTime%2522%253A1689083941751%257D';
//         }).as("popup")
//     })
//     cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
// })


// When('Clicar no botão Google', function () {
//     cy.clickGoogle();
// })
// When('Sign In no Google', function () {
//     //  cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
//     cy.task('db:seed')
//     cy.loginByGoogleApi()
//     cy.contains('Get Started').should('be.visible')
// })

// When('Sign In no Google', function () {
//     //  cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
//     cy.task('db:seed')
//     cy.loginByGoogleApi()
//     cy.contains('Get Started').should('be.')
// })


// When("Aceder a página pública {string}", function (pagina) {
//     if (pagina == "Find Spot to invest") {
//         cy.visit("https://qa.investinportugal.pt/find-spot-to-invest/")
//     } else if (pagina == "Portugal in the World") {
//         cy.visit("https://qa.investinportugal.pt/portugal-in-the-world/")

//     }
// })

// When("Validar módulo {string}", function (modulo) {

//     cy.log('ok')
//     if (modulo == "Simple Header") {
//         cy.get('#iFrame1').then(($iframe) => {
//             const $body = $iframe.contents().find('.simpleHeader')
//         })
//     } else if (modulo == "Find Spot Form") {
//         cy.get('#iFrame1').then(($iframe) => {
//             const $body = $iframe.contents().find('.findSpotForm')
//         })
//         //     cy.get('.findSpotForm').should('be.visible')

//     }

// })