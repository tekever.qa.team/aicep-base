/// <reference types="Cypress" />
///<reference types="cypress-iframe" />

import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
let arealink = null;
let projetolink = null;
let dotlink = null;

Given("Aceder à área {string} do {string}", function (area, projeto) {

    if (projeto == 'IIPT') {
        projetolink = 'investinportugal';
        dotlink = 'pt';
    } else {
        projetolink = 'buyfromportugal';
        dotlink = 'com';
    }

    if (area == 'Privada') {
        arealink = '-myaicep';
    } else {
        arealink = '';
    }
    cy.log('https://' + Cypress.env('SERVER') + arealink + '.' + projetolink + '.' + dotlink)
    cy.visit('https://' + Cypress.env('SERVER') + arealink + '.' + projetolink + '.' + dotlink);
});

When('Preencher Email e Password', function () {
    // const i = randomInteger(0, 1);
    cy.fixture('data/userFile.json')
        .then((userFile) => {
            cy.loginPrivateIIPT(userFile.userIIPT[1].email, userFile.userIIPT[1].password)
        })
})


Then('Clicar no botão Connect', function () {
    cy.clickBtnConnectMe();
})

Then('Visualizar homepage da área privada', function () {
    cy.welcomeIIPT();
})

When('Clicar no botão LinkedIn', function () {

    cy.clickLinkedin();
})

When('Sign In no LinkedIn', function () {
    cy.url().then(url =>
        cy.visit(url, {
            onBeforeLoad(win) {
                // stub window.open with a custom version
                cy.stub(win, 'open', (url, target, features) => {
                    console.warn('window.open was called: ', { url, target, features })
                    // our custom window.open that allows cypress to switch between the windows ("called tabs in the helpers")
                    cy.openTab(url, {
                        // make this something you can call later to switch Cypress contexts between the parent window and the new window
                        // this is required
                        tab_name: 'MySecondWindow',

                        // these can control window size and position
                        window_features: features,
                    })

                    return true;
                }).as('windowOpen')
            }
        }))
    cy.clickLinkedin();
    cy.window().then(window => {
        //window.callMyJSThatCallsWindowOpen()
        cy.loginLinkedin('amatheus.ssa@gmail.com', 'C1rinarissi');
        // cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
        cy.singnInLinkedin()
        // cy.closeTab('MySecondWindow')
    })




    // const oldUrl = cy.url();
    // const newUrl = 'https://www.linkedin.com/uas/login?session_redirect=%2Foauth%2Fv2%2Flogin-success%3Fapp_id%3D207679828%26auth_type%3DAC%26flow%3D%257B%2522scope%2522%253A%2522r_liteprofile%2Br_emailaddress%2522%252C%2522state%2522%253A%2522_linkedin%2522%252C%2522appId%2522%253A207679828%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522https%253A%252F%252Fqa-myaicep.investinportugal.pt%252Flinkedin%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522currentSubStage%2522%253A0%252C%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522creationTime%2522%253A1689792969184%257D&fromSignIn=1&trk=oauth&cancel_redirect=%2Foauth%2Fv2%2Flogin-cancel%3Fapp_id%3D207679828%26auth_type%3DAC%26flow%3D%257B%2522scope%2522%253A%2522r_liteprofile%2Br_emailaddress%2522%252C%2522state%2522%253A%2522_linkedin%2522%252C%2522appId%2522%253A207679828%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522https%253A%252F%252Fqa-myaicep.investinportugal.pt%252Flinkedin%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522currentSubStage%2522%253A0%252C%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522creationTime%2522%253A1689792969184%257D'

    // cy.window().then(win => {
    //     cy.stub(win, 'open').as('windowOpen');
    // });

    // cy.clickLinkedin();
    // cy.get('@windowOpen')
    // cy.window().then(win => {
    //     win.location.href = newUrl;
    // });

    // cy.loginLinkedin('amatheus.ssa@gmail.com', 'C1rinarissi');
    // cy.window().then(win => {
    //     cy.stub(win, 'open').as('windowOpen');
    // });
    // cy.singnInLinkedin()
    // cy.get('@windowOpen')
    // cy.window().then(win => {
    //     win.location.href = oldUrl;
    // })

    //     cy.window().then(win => {
    //         win.location.href = oldUrl;
    //     });
    // })
    //     // Get window object
    //     cy.window().then((win) => {
    //         // Replace window.open(url, target)-function with our own arrow function
    //         cy.stub(win, 'open', url => {
    //             // change window location to be same as the popup url
    //             win.location.href = 'https://www.linkedin.com/uas/login?session_redirect=%2Foauth%2Fv2%2Flogin-success%3Fapp_id%3D207679828%26auth_type%3DAC%26flow%3D%257B%2522scope%2522%253A%2522r_liteprofile%2Br_emailaddress%2522%252C%2522state%2522%253A%2522_linkedin%2522%252C%2522appId%2522%253A207679828%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522https%253A%252F%252Fqa-myaicep.investinportugal.pt%252Flinkedin%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522currentSubStage%2522%253A0%252C%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522creationTime%2522%253A1689792969184%257D&fromSignIn=1&trk=oauth&cancel_redirect=%2Foauth%2Fv2%2Flogin-cancel%3Fapp_id%3D207679828%26auth_type%3DAC%26flow%3D%257B%2522scope%2522%253A%2522r_liteprofile%2Br_emailaddress%2522%252C%2522state%2522%253A%2522_linkedin%2522%252C%2522appId%2522%253A207679828%252C%2522authorizationType%2522%253A%2522OAUTH2_AUTHORIZATION_CODE%2522%252C%2522redirectUri%2522%253A%2522https%253A%252F%252Fqa-myaicep.investinportugal.pt%252Flinkedin%2522%252C%2522currentStage%2522%253A%2522LOGIN_SUCCESS%2522%252C%2522currentSubStage%2522%253A0%252C%2522authFlowName%2522%253A%2522generic-permission-list%2522%252C%2522creationTime%2522%253A1689792969184%257D'

    //         }).as("popup") // alias it with popup, so we can wait refer it with @popup
    //     })
    //     cy.get('@popup')
    //     cy.get('input').first().focus()

    //     cy.get('#app__container').should('be.visible')
    //     cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
    // })
})




When('Clicar no botão Google', function () {
    cy.clickGoogle();
})
When('Sign In no Google', function () {
    //  cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
    cy.task('db:seed')
    cy.loginByGoogleApi()
    cy.contains('Get Started').should('be.visible')
})

When('Sign In no Google', function () {
    //  cy.loginLinkedin('amanda.matheus@tekever.com', 'Aicep2023@test');
    cy.task('db:seed')
    cy.loginByGoogleApi()
    cy.contains('Get Started').should('be.')
})


When("Aceder a página pública {string}", function (pagina) {

    cy.visitLink('publica', pagina)
    cy.wait(6000)

})

When("Clicar no menu {string}", function (pagina) {
    cy.wait(2000)
    cy.visitLink('privada', pagina)
    cy.wait(3000)

    // if (pagina == "Find Spot To Invest") {
    //     cy.get('ul.mainNav__firstLevelList').contains(pagina).click()
    // } else {
    //     cy.get('.mainNav__firstLevelList > :nth-child(1)').click()
    //     cy.contains(pagina).click()
    // }
})

When("Validar módulo {string} da página pública", function (modulo) {

    const moduloSemEspaco = modulo.replaceAll(' ', '')
    const moduloReduzido = moduloSemEspaco[0].toLowerCase() + moduloSemEspaco.substr(1)
    cy.iframeElementShouldBeVisible(moduloReduzido)

})

When("Validar módulo {string}", function (modulo) {

    const moduloSemEspaco = modulo.replaceAll(' ', '')
    const moduloReduzido = moduloSemEspaco[0].toLowerCase() + moduloSemEspaco.substr(1)
    cy.log(moduloReduzido)
    cy.elementShouldBeVisible(moduloReduzido)
})

When("Validar um segundo módulo {string}", function (modulo) {

    const moduloSemEspaco = modulo.replaceAll(' ', '')
    const moduloReduzido = moduloSemEspaco[0].toLowerCase() + moduloSemEspaco.substr(1)
    cy.twoElementShouldBeVisible(moduloReduzido)


})
