// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.on('uncaught:exception', function (err, runnable) {
    return false
})

Cypress.Commands.add('loginPrivateIIPT', function (email, password) {
    cy.fixture('IIPT/private/login_private.json').then((locator) => {
        cy.get(locator.inputEmail).type(email)
        cy.get(locator.inputPassword).type(password)
    })
})

Cypress.Commands.add('welcomeIIPT', function () {
    cy.fixture('IIPT/private/header_private.json').then((locator) => {
        cy.get(locator.btnWelcome)
            .contains('Welcome')
    })
})

Cypress.Commands.add('clickLinkedin', function () {
    cy.fixture('IIPT/private/login_private.json').then((locator) => {
        cy.get(locator.btnLinkedin)
            .click()
    })
})

Cypress.Commands.add('loginLinkedin', function (email, password) {
    cy.fixture('IIPT/private/login_private.json').then((locator) => {
        cy.get(locator.inputEmailLinkedin).type(email)
        cy.get(locator.inputPasswordLinkedin).type(password)
        // cy.get(locator.btnSignInLinkedin).click()

    })
})

Cypress.Commands.add('singnInLinkedin', function () {
    cy.fixture('IIPT/private/login_private.json').then((locator) => {

        cy.get(locator.btnSignInLinkedin).click()
        // cy.get(locator.btnPermitirInLinkedin).click()
    })
})

Cypress.Commands.add('clickBtnConnectMe', function () {
    cy.fixture('IIPT/private/login_private.json').then((locator) => {
        cy.get(locator.btnConnectMe).click()
    })

})

Cypress.Commands.add('search', function (text) {
    cy.fixture('IIPT/private/header_private.json').then((locator) => {
        cy.wait(3000)
        cy.get(locator.btnSearch).click()
        cy.get(locator.inputSearch).type(text + '{enter}')
    })
})



Cypress.Commands.add('clickGoogle', function () {
    cy.fixture('IIPT/private/login_private.json').then((locator) => {
        cy.get(locator.btnGoogle)
            .click()
    })
})

Cypress.Commands.add('iframeElementShouldBeVisible', function (moduloReduzido) {

    cy.fixture('IIPT/modulos.json').then((locator) => {
        cy.frameLoaded(locator.iframe)

    })
    cy.fixture('IIPT/modulos.json').its('modulos').then(mod => {
        for (var i = 0; i <= mod.length; i++) {

            if (mod.nomeModulo == moduloReduzido) {
                cy.log('entrou ' + mod.nomeModulo)
                cy.iframe().find(mod.elemento).should('be.visible')
                return false;
            }
        }
    })
})

Cypress.Commands.add('elementShouldBeVisible', function (moduloReduzido) {

    cy.fixture('IIPT/modulos.json').its('modulos').then(mod => {

        for (var i = 0; i <= mod.length; i++) {

            if (mod[i].nomeModulo == moduloReduzido) {
                cy.get(mod[i].elemento).should('be.visible')
                return false
            }
        }
    })
})

Cypress.Commands.add('twoElementShouldBeVisible', function (moduloReduzido) {

    cy.fixture('IIPT/modulos.json').its('modulos').then(mod => {
        for (var i = 0; i <= mod.length; i++) {
            if (mod.nomeModulo == moduloReduzido) {
                cy.document().then((doc) => {
                    const itensCount = doc.querySelectorAll(mod.elemento).length
                    expect(itensCount).to.eq(2)
                    return false
                })
            }
        }
    })

})

Cypress.Commands.add('visitLink', function (area, pagina) {

    cy.fixture('IIPT/link.json').its('links').then(lin => {

        for (var i = 0; i <= lin.length; i++) {
            cy.log(lin[i].nomePagina)
            if (lin[i].nomePagina == pagina) {

                if (area == 'privada') {
                    cy.visit("https://" + Cypress.env('SERVER') + lin[i].linkPrivado)
                } else {
                    cy.visit("https://" + Cypress.env('SERVER') + lin[i].linkPublico)
                }
                return false
            }
        }
    })
})