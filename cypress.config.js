const { defineConfig } = require('cypress');

const createBundler = require('@bahmutov/cypress-esbuild-preprocessor');
const preprocessor = require('@badeball/cypress-cucumber-preprocessor');
const createEsbuildPlugin = require('@badeball/cypress-cucumber-preprocessor/esbuild');

const setupNodeEvents = async (on, config) => {
    await preprocessor.addCucumberPreprocessorPlugin(on, config);
    on(
        'file:preprocessor',
        createBundler({
            plugins: [createEsbuildPlugin.default(config)],
        }),
    );
    return config;
};


module.exports = defineConfig({
    env: {
        IIPT_PRI: "https://qa-myaicep.investinportugal.pt/", //"https://stg-myaicep.investinportugal.pt/",
        IIPT_PUB: "https://qa.investinportugal.pt/", //"https://stg.investinportugal.pt/"
        SERVER: "qa"
    },
    viewportWidth: 1920,
    viewportHeight: 1080,
    defaultCommandTimeout: 20000,
    chromeWebSecurity: false,
    e2e: {
        hideXHRInCommandLog: true,
        setupNodeEvents,
        specPattern: '**/*.feature',
        excludeSpecPattern: ['*.js'],
    },
});
