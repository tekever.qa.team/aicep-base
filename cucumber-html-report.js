const report = require("multiple-cucumber-html-reporter");
report.generate({
    jsonDir: "cypress/cucumber-report",  // ** Path of .json file **//
    reportPath: "cypress/cucumber-report/cucumber-htmlreport.html",
    metadata: {
        browser: {
            name: "chrome",
            version: "114",
        },
        device: "Local test machine",
        platform: {
            name: "windows",
            version: "Catalina",
        },
    },
});